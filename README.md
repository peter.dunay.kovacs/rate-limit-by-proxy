# What to make of this

1. Start nginx by executing something like:

   ```
   docker run --rm --name nginx-echo -v $PWD/nginx.conf:/etc/nginx/nginx.conf:ro -p 9091:80 nginx
   ```

1. Run the `test.sh` script by executing something like:

   ```
   $ bash test.sh 2>&1 | tee log
   ```

It will be running for about 5 seconds with the following results:

```
$ grep -c 'HTTP/1.1 200' log
9
$ grep -c 'HTTP/1.1 503' log
491
```

This is because the `burst=1` nginx setting will allow to run about 2 requests per second without overflow: `5 secs * 2 req/sec ~ 9` successful requests (one request will be sent immediately, one will be queued); the rest will overflow (`HTTP 503`).

This might be relevant when looking for configuration options: https://www.nginx.com/blog/rate-limiting-nginx/#comment-4985916641 .
