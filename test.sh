#!/bin/bash

for n in $(seq 1 50);
do
    cat <<EOF

===========
$n...
===========
EOF
    for inner in $(seq 1 10);
    do
        cat <<EOF

    -----------
    $inner...
    -----------
EOF
        curl -i http://$(my_ip):9091/echo &
    done
    sleep 0.1
done
